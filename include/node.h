#ifndef NODE_H
#define NODE_H

#include <vector>
#include <string>

class node
{
	public:
		/** Default constructor */
		node();
		/** Default destructor */
		virtual ~node();

		std::vector<std::string> m_params; //!< Member variable "m_params"

	protected:

	private:
};

#endif // NODE_H
