#include <tinyxml2.h>
#include <iostream>
#include <vector>
#include <string>
#include "config.h"

#ifndef XMLCheckResult
	#define XMLCheckResult(a_eResult) if (a_eResult != XML_SUCCESS) { printf("Error: %i\n", a_eResult); return a_eResult; }
#endif

using namespace tinyxml2;

int main(){
	XMLDocument xmlDoc;
	XMLError eResult=xmlDoc.LoadFile("config.xml");
	XMLCheckResult(eResult);
	XMLNode *pRoot=xmlDoc.FirstChild();
	if(pRoot==nullptr) return XML_ERROR_FILE_READ_ERROR;
}
